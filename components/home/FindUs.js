import { FindPlaceView } from "./HomeBanner";

export default function FindUs() {
  return (
    <>
      <div className="partner bg-gray mb-ssm py-ssm ">
        <div className="how-it-works mb-ssm ">
          <div className="container">
            <div className="row">
              <div className="col-12 col-md-11 col-lg-11 col-xl-10 col-xxl-10 mx-auto">
                <div className="row">
                  <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                    <div className="main-heading text-center mb-ssm">
                      <h2 className="fw-bold mb-1">Partner With Parknite</h2>
                      <h5 className="fw-normal mb-0">
                        List your property with us{" "}
                      </h5>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-12 col-md-6 col-lg-6 col-xl-5 col-xxl-5">
                    <div className="work-right-content position-relative">
                      <img
                        src="../assets/img/img3.png"
                        className="img-fluid"
                        alt="..."
                      />

                      <div className="position-absolute content-position  bg-card text-center">
                        <h4 className="mb-0 heading-font">3.5k+</h4>
                        <p className="mb-0">Registered users</p>
                      </div>
                      <div className="position-absolute content-position1  bg-card">
                        <svg
                          aria-hidden="true"
                          focusable="false"
                          data-prefix="fas"
                          data-icon="info-circle"
                          role="img"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 512 512"
                          className="svg-inline--fa fa-info-circle fa-w-16 fa-2x">
                          <path
                            fill="currentColor"
                            d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 110c23.196 0 42 18.804 42 42s-18.804 42-42 42-42-18.804-42-42 18.804-42 42-42zm56 254c0 6.627-5.373 12-12 12h-88c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h12v-64h-12c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h64c6.627 0 12 5.373 12 12v100h12c6.627 0 12 5.373 12 12v24z"
                            className
                          />
                        </svg>
                        <p className="mb-0  ms-2 d-inline-block">
                          Know more about partner with us
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-12 col-md-6 col-lg-6 col-xl-5 col-xxl-5 offset-xl-2 offset-xxl-2  my-auto">
                    <div className="work-left-content">
                      {[1, 2, 3].map((val, index) => (
                        <FindPlaceView
                          count={index + 1}
                          title={"Find place of your choice"}
                          subtitle={
                            "Search from various available spots all around the place that suits you."
                          }
                        />
                      ))}
                      <button className="btn btn-primary btn-xl w-50">
                        Start your search
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="find-us mb-ssm">
        <div className="container">
          <div className="row">
            <div className="col-12 col-md-12 col-lg-12 col-xl-12">
              <div className="main-heading text-center mb-ssm">
                <h2 className="fw-bold mb-1">Find us wherever you go</h2>
                <h5 className="fw-normal">
                  We got properties for you all around.
                </h5>
                <div className="mt-3 mb-3">
                  <img src="../assets/img/img11.png" />
                </div>
                <button className="btn btn-primary btn-xl w-auto">
                  Explore all properties
                </button>
              </div>
            </div>
          </div>
          <hr />
        </div>
      </div>
      <div className="safety mb-ssm">
        <div className="container">
          <div className="row">
            <div className="col-12 col-md-12 col-lg-12 col-xl-12">
              <div className="main-heading text-center mb-ssm">
                <h2 className="fw-bold mb-1">Parknite safety</h2>
                <h5 className="fw-normal">
                  Feel free to travel, we will take care of your safety.
                </h5>
              </div>
            </div>
          </div>
          <div className="row">
            {[1, 2, 3, 4].map((val) => (
              <div className="col-12 col-md-3 col-lg-3 col-xl-3 col-xxl-3">
                <div className="bg-card card-info text-center">
                  <div className="my-3">
                    <svg
                      aria-hidden="true"
                      focusable="false"
                      data-prefix="fal"
                      data-icon="credit-card-front"
                      role="img"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 576 512"
                      className="svg-inline--fa fa-credit-card-front fa-w-18 fa-2x">
                      <path
                        fill="currentColor"
                        d="M528 31H48C21.5 31 0 52.5 0 79v352c0 26.5 21.5 48 48 48h480c26.5 0 48-21.5 48-48V79c0-26.5-21.5-48-48-48zm16 400c0 8.8-7.2 16-16 16H48c-8.8 0-16-7.2-16-16V79c0-8.8 7.2-16 16-16h480c8.8 0 16 7.2 16 16v352zm-352-68v8c0 6.6-5.4 12-12 12h-72c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h72c6.6 0 12 5.4 12 12zm192 0v8c0 6.6-5.4 12-12 12H236c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h136c6.6 0 12 5.4 12 12zM488 95h-80c-13.3 0-24 10.7-24 24v48c0 13.3 10.7 24 24 24h80c13.3 0 24-10.7 24-24v-48c0-13.3-10.7-24-24-24zm-8 64h-64v-32h64v32zM260 319h-56c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h56c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm28-12v-40c0-6.6 5.4-12 12-12h56c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12h-56c-6.6 0-12-5.4-12-12zm-192 0v-40c0-6.6 5.4-12 12-12h56c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12h-56c-6.6 0-12-5.4-12-12zm384-40v40c0 6.6-5.4 12-12 12h-72c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h72c6.6 0 12 5.4 12 12z"
                        className
                      />
                    </svg>
                  </div>
                  <h5 className="mb-2">Secure payments</h5>
                  <p>
                    Don’t worry about you payment details, we make it secure
                    &amp; safe.
                  </p>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
      <div className="know-more bg-gray py-5">
        <div className="container">
          <div className="row">
            <div className="col-12 col-md-12 col-lg-12 col-xl-12">
              <div className="main-heading text-center mb-ssm">
                <h2 className="fw-bold mb-1">Want to know more about us </h2>
                <h5 className="fw-normal">
                  Feel free to ask any question, our team will get back to you
                  with answers you need.
                </h5>
                <div className="row">
                  <div className="col-12 col-md-5 col-lg-5 col-xl-5 col-xxl-5 mx-auto">
                    <div className="input-group-btn1 mt-3">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Your email address"
                      />
                      <button className="btn btn-primary btn-xl w-auto">
                        Subscribe
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
