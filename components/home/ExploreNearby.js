import React from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { carousalConfig } from "../common/Constants";
import { useSnackbar, withSnackbar } from "notistack";

class ExploreNearby extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentSlideIndex: 0,
      cardData: [
        {
          src: "../assets/img/img5.png",
          title: "Enjoy the great cold",
          subtitle: "6,879 properties",
        },
        {
          src: "../assets/img/img5.png",
          title: "Enjoy the great cold",
          subtitle: "6,879 properties",
        },
        {
          src: "../assets/img/img6.png",
          title: "Unique stays",
          subtitle: "6,879 properties",
        },
      ],
      nearbyData: [
        {
          src: "../assets/img/img7.jpg",
          title: "Beach",
          subtitle: "15 properties",
        },
        {
          src: "../assets/img/img7.jpg",
          title: "Mountains",
          subtitle: "15 properties",
        },
        {
          src: "../assets/img/img7.jpg",
          title: "Dessert",
          subtitle: "15 properties",
        },
        {
          src: "../assets/img/img7.jpg",
          title: "Forrest",
          subtitle: "15 properties",
        },
      ],
      destinationData: [
        {
          src: "../assets/img/img8.png",
          title: "Shenandoah National Park",
          distance: "220 mi",
          subtitle: "Starts from 50 USD",
        },
      ],
    };
  }

  ButtonGroup = (props) => {
    return (
      <div className="carousel-button-group">
        <div className="slider_nav">
          <button
            className="button-border-clear"
            onClick={() => {
              this.setState(
                {
                  currentSlideIndex:
                    this.state.currentSlideIndex != 0
                      ? this.state.currentSlideIndex - 1
                      : 0,
                },
                () => {
                  this.Carousel.goToSlide(this.state.currentSlideIndex);
                }
              );
            }}>
            <div className="am-prev">
              <img
                src="../assets/img/left.png"
                className="img-fluid"
                alt="..."
              />
            </div>
          </button>
          <button
            className="button-border-clear"
            onClick={() => {
              this.setState(
                {
                  currentSlideIndex:
                    this.state.currentSlideIndex <
                    this.state.nearbyData.length - 1
                      ? this.state.currentSlideIndex + 1
                      : this.state.currentSlideIndex,
                },
                () => {
                  this.Carousel.goToSlide(this.state.currentSlideIndex);
                }
              );
            }}>
            <div className="am-next">
              <img
                src="../assets/img/right.png"
                className="img-fluid"
                alt="..."
              />
            </div>
          </button>
        </div>
      </div>
    );
  };

  render() {
    return (
      <>
        <div className="move-anywhere mb-ssm">
          <div className="container">
            <div className="row">
              <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                <div className="main-heading text-center mb-ssm">
                  <h2 className="fw-bold mb-1">Move Anywhere</h2>
                  <h5 className="fw-normal mb-0">Relax and travel on</h5>
                </div>
              </div>
            </div>
            <div className="row">
              {this.state.cardData.map((item, index) => (
                <CardView key={item.title} item={item} />
              ))}
            </div>
          </div>
        </div>
        <div className="explore-near bg-gray mb-ssm py-ssm ">
          <div className="container">
            <div className="row">
              <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                <div className="main-heading text-center mb-ssm">
                  <h2 className="fw-bold mb-1">Explore Nearby</h2>
                  <h5 className="fw-normal mb-0">
                    Find best places to park on
                  </h5>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-md-11 col-lg-11 col-xl-10 col-xxl-10 mx-auto">
                <div className="row  justify-content-center">
                  {this.state.nearbyData.map((item, index) => (
                    <NearbyCardView key={item.title} item={item} />
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="destination mb-ssm">
          <div className="container">
            <div className="row">
              <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                <div className="main-heading mb-ssm">
                  <h2 className="fw-bold mb-1">Top destinations</h2>
                  <h5 className="fw-normal mb-0">
                    Discover unique spots near you to enjoy your day.
                  </h5>
                </div>
              </div>
            </div>
            <div className="row">
              {this.state.nearbyData.map((item, index) => (
                <DestinationCardView key={item.title} item={item} />
              ))}
            </div>
          </div>
        </div>

        <div className="traveller mb-ssm">
          <div className="container">
            <div className="row">
              <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                <div className="main-heading text-center mb-ssm">
                  <h2 className="fw-bold mb-1">Traveller’s choice</h2>
                  <h5 className="fw-normal mb-0">50+ beautiful places to go</h5>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-md-11 col-lg-11 col-xl-10 col-xxl-10 mx-auto">
                <Carousel
                  ref={(el) => (this.Carousel = el)}
                  showDots={false}
                  infinite={false}
                  arrows={false}
                  keyBoardControl={true}
                  responsive={carousalConfig}
                  containerClass={"owl-carousel"}
                  ssr={true}>
                  {this.state.nearbyData.map((item, intex) => (
                    <CarouselView key={item.title} />
                  ))}
                </Carousel>

                <this.ButtonGroup />
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export const CardView = (props) => {
  return (
    <div className="col-12 col-md-4 col-lg-4 col-xl-4 col-xxl-4">
      <div className="move-content">
        <img src={props.item.src} className="img-fluid" alt="..." />
        <div className="mt-3 text-center">
          <h5>{props.item.title}</h5>
          <h6 className="fw-normal mb-0">{props.item.subtitle}</h6>
        </div>
      </div>
    </div>
  );
};

export const NearbyCardView = (props) => {
  return (
    <div className="col-width">
      <div className="bg-card mb-3 card-explore">
        <div className="pt-2 p-ssm">
          <small>{props.item.subtitle}</small>
        </div>
        <div className="in-card-explore text-center">
          <div className="in-card-img mb-3">
            <img src={props.item.src} className="img-fluid" alt="..." />
          </div>
          <h5 className="fw-normal mb-2">{props.item.title}</h5>
        </div>
      </div>
    </div>
  );
};

export const DestinationCardView = (props) => {
  return (
    <div className="col-12 col-md-3 col-lg-3 col-xl-3 col-xxl-3">
      <div className="card-details pb-m">
        <div className="card-img">
          <img src="../assets/img/img8.png" />
        </div>
        <div className="card-body">
          <div className="bg-card">
            <div className="card-padding">
              <h5 className="mb-1">Shenandoah National Park</h5>
              <p className="fw-normal mb-2">220 mi</p>
              <h6 className="fw-normal">Starts from 50 USD</h6>
            </div>
            <button type="button" className="btn btn-blue">
              Book now
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export const CarouselView = (props) => {
  return (
    <div className="slider">
      <div className="row">
        <div className="col-12 col-md-6 col-lg-6 col-xl-5 col-xxl-5">
          <div className="slider-items">
            <div className="react blue" />
            <div className="react orange" />
            <h3>
              It’s a grate experience to Parknite, they have some of the best
              places to visit with lots of amenities and great surrounding
            </h3>
          </div>
        </div>
        <div className="col-12 col-md-6 col-lg-6 col-xl-5 col-xxl-5 offset-xl-2 offset-xxl-2">
          <div className="slider-img">
            <img
              src="../assets/img/img10.png"
              className="img-fluid"
              alt="..."
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default withSnackbar(ExploreNearby);
