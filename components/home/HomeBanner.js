import Carousel from "react-multi-carousel";
import { carousalConfig } from "../common/Constants";

export default function HomeBanner(props) {
  const nearbyData = [
    {
      src: "../assets/img/img2.png",
      title: "Beach",
      subtitle: "15 properties",
    },
    {
      src: "../assets/img/img2.png",
      title: "Mountains",
      subtitle: "15 properties",
    },
    {
      src: "../assets/img/img2.png",
      title: "Dessert",
      subtitle: "15 properties",
    },
    {
      src: "../assets/img/img2.png",
      title: "Forrest",
      subtitle: "15 properties",
    },
  ];

  return (
    <>
      <div className="home-banner mb-ssm ">
        <Carousel
          showDots={false}
          swipeable={false}
          arrows={false}
          transitionDuration={500}
          keyBoardControl={true}
          responsive={carousalConfig}
          autoPlay={true}
          autoPlaySpeed={3000}
          containerClass="carousel slide"
          ssr={true}>
          {nearbyData.map((item, intex) => (
            <img src={item.src} className="img-fluid d-block w-100" alt="..." />
          ))}
        </Carousel>
        <div className="home-content">
          <div className="container">
            <div className="row">
              <div className="col-12 col-sm-4">
                <div className="contents">
                  <h1 className="fw-bold">
                    Find Best <br /> Place For <br /> RV camping
                  </h1>
                  <a href="#">Explore all</a>
                  <div className="arrow" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="how-it-works mb-ssm ">
        <div className="container">
          <div className="row ">
            <div className="col-12 col-md-11 col-lg-11 col-xl-10 col-xxl-10 mx-auto">
              <div className="row">
                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                  <div className="main-heading text-center mb-ssm">
                    <h2 className="fw-bold mb-1">How It Works</h2>
                    <h5 className="fw-normal mb-0">
                      Have a look at our process
                    </h5>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12 col-md-6 col-lg-6 col-xl-5 col-xxl-5 my-auto">
                  <div className="work-left-content">
                    {[1, 2, 3].map((val, index) => (
                      <FindPlaceView
                        count={index + 1}
                        title={"Find place of your choice"}
                        subtitle={
                          "Search from various available spots all around the place that suits you."
                        }
                      />
                    ))}

                    <button className="btn btn-primary btn-lg w-50">
                      Start your search
                    </button>
                  </div>
                </div>
                <div className="col-12 col-md-6 col-lg-6 col-xl-5 col-xxl-5 offset-xl-2 offset-xxl-2">
                  <div className="work-right-content position-relative">
                    <img
                      src="../assets/img/img3.png"
                      className="img-fluid"
                      alt="..."
                    />
                    <TagView greenText={"500+"} blackText={"Properties"} />
                    <div className="position-absolute  content-position1  bg-card">
                      <svg viewBox="0 0 512 512">
                        <path
                          fill="currentColor"
                          d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm115.7 272l-176 101c-15.8 8.8-35.7-2.5-35.7-21V152c0-18.4 19.8-29.8 35.7-21l176 107c16.4 9.2 16.4 32.9 0 42z"
                          className
                        />
                      </svg>
                      <p className="mb-0  ms-2 d-inline-block">
                        Watch
                        <br />
                        How it works
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export const FindPlaceView = (props) => {
  return (
    <div className="in-content mb-4">
      <h6 className="mb-2">{props.count}</h6>
      <h4 className="mb-2">{props.title}</h4>
      <p className="mb-0">{props.subtitle}</p>
    </div>
  );
};

export const TagView = (props) => {
  return (
    <div className="position-absolute content-position  bg-card text-center">
      <h4 className="mb-0 heading-font">{props.greenText}</h4>
      <p className="mb-0">{props.blackText}</p>
    </div>
  );
};
