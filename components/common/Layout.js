import Footer from "./Footer";
import Header from "./Header";

const Layout = ({ children }) => (
  <>
    <Header isOverlapping={true} />
    {children}
    <Footer />
  </>
);

export default Layout;
