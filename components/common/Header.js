import Link from "next/link";

export default function Header(props) {
  return (
    <div className="header">
      <div
        className={
          props.isOverlapping
            ? props.showBanner
              ? "banner-header"
              : "home-header"
            : "none"
        }>
        <nav className="navbar navbar-expand-lg">
          <div className="container">
            <a className="navbar-brand" href="#">
              <img
                src="../assets/img/logo-bg.png"
                className="img-fluid"
                alt="..."
              />
            </a>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation">
              <span className="navbar-toggler-icon" />
            </button>
            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent">
              <ul className="navbar-nav  ms-auto mb-2 mb-lg-0">
                <li key={"aboutus"} className="nav-item">
                  <a className="nav-link active" aria-current="page" href="#">
                    About us
                  </a>
                </li>
                <li key={"explore"} className="nav-item">
                  <Link passHref={true} href={"/explore"}>
                    <a className="nav-link">Explore</a>
                  </Link>
                </li>
                <li key={"becomeHost"} className="nav-item">
                  <a className="nav-link" href="#">
                    Become a host
                  </a>
                </li>
                <li key={"howItWorks"} className="nav-item">
                  <a className="nav-link" href="#">
                    How it works
                  </a>
                </li>
                <li key={"contact"} className="nav-item">
                  <a className="nav-link" href="#">
                    Contact
                  </a>
                </li>
                <li key={"login"} className="nav-item">
                  <a className="nav-link" href="../form/login.html">
                    Login
                  </a>
                </li>
                <li key={"signup"} className="nav-item">
                  <a className="btn btn-primary btn-lg" href="signup.html">
                    Signup
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </div>
  );
}
