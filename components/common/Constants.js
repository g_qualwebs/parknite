export const carousalConfig = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 1,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 1,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 1,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
};

export const customStylesDropdown = {
  option: (provided, state) => ({
    ...provided,
    color: "#000000",
  }),
  container: (provided, state) => ({
    ...provided,
    zIndex: 1000,
  }),
  control: (provided) => ({
    ...provided,
    marginTop: "0%",
    width: "150",
    backgroundColor: "transparent",
    border: "none",
    width: 150,
    boxShadow: "none",
    marginLeft: -10,
    marginBottom: -15,
    zIndex: 1000,
  }),
  placeholder: (provided, state) => ({
    ...provided,
    color: "#ffffff",
    fontSize: 14,
    marginTop: -15,
  }),
  dropdownIndicator: (provided, state) => ({
    ...provided,
    width: 0,
    color: "transparent",
  }),
  indicatorsContainer: (provided, state) => ({
    ...provided,
    width: 0,
    color: "transparent",
  }),
  singleValue: (provided, state) => ({
    ...provided,
    color: "#ffffff",
    marginTop: -15,
    fontSize: 14,
  }),
  menuList: (provided, state) => ({
    ...provided,
    zIndex: 1000,
    backgroundColor: "white",
    border: "1px solid lightgray",
  }),
};
