import React from "react";
import Carousel from "react-multi-carousel";
import { carousalConfig } from "../../common/Constants";

export default class PropertyDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      featureData: [
        {
          id: 1,
          title: "Lodging Provided",
          data: [
            "Canvas tent",
            "I site",
            "Upto 4 guest per site",
            "Short walk",
            "Wheelchair access",
          ],
        },
        {
          id: 2,
          title: "Essentials",
          data: ["Campfires allowed", "Toilet available", "Pets allowed"],
        },
        {
          id: 3,
          title: "Amenities",
          data: [
            "Showers available",
            "Picnic table available",
            "Bins allowed",
            "No potable water",
            "No kitchen",
            "No wifi",
            "Laundry absent",
          ],
        },
      ],
    };
  }

  GeneralInfoView = (props) => {
    return (
      <div className="row">
        <div className="col-6 col-sm-6 col-md-6">
          <h6 className="fw-normal">{props.name}</h6>
        </div>
        <div className="col-6 col-sm-6 col-md-6">
          <p>{props.value}</p>
        </div>
      </div>
    );
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-12 col-md-9 col-lg-9 col-xl-9 col-xxl-9 mt-5">
            <div className="bio-details about-property mb-4">
              <h3 className="fw-bold">More About Property</h3>
              <h5 className="fw-normal  gray-title">
                Come be a part of a growing community at The Barking Owl. Owned
                and operated by climbers. Stunning sunsets, perfect sunrises.
                The campsites are on a beautiful piece of land with over 30
                mature Joshua Trees in the sleepy neighborhood of Flamingo
                Heights. 17 minutes from Joshua Tree national Park.
              </h5>
              <h5 className="fw-normal  gray-title">
                The Barking Owl is a place for climbers of all skill sets to
                connect, share, and learn. All are welcome, not just climbers.
                <a href="#">Read more</a>
              </h5>
            </div>
            <hr />
            <div className="row">
              {this.state.featureData.map((item, index) => (
                <div className="col-12 col-md-6 col-lg-4 col-xl-4 col-xxl-4 vertical-border">
                  <div className="essential-info">
                    <h4 className="mb-3">{item.title}</h4>
                    <ul>
                      {item.data.map((val) => (
                        <li>
                          <p className="mb-2">{val}</p>
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>
              ))}
            </div>
            <hr />
            <div className="property-details">
              <h4 className="mb-3">Property details</h4>
              <div className="row">
                <div className="col-12 col-sm-6 col-md-6 vertical-border">
                  <this.GeneralInfoView
                    name={"Check In: "}
                    value={"After 2:00pm"}
                  />
                  <this.GeneralInfoView
                    name={"Check Out: "}
                    value={"Before 10:00am"}
                  />
                  <this.GeneralInfoView
                    name={"Cancellation Policy: "}
                    value={"Guest friendly"}
                  />
                </div>
                <div className="col-12 col-sm-6 col-md-6 vertical-border">
                  <this.GeneralInfoView
                    name={"On arrival booking: "}
                    value={"Available"}
                  />
                  <this.GeneralInfoView
                    name={"Min. booking days: "}
                    value={"Before 10:00am"}
                  />
                  <this.GeneralInfoView
                    name={"How early can you book: "}
                    value={"Guest friendly"}
                  />
                </div>
              </div>
            </div>
            <hr />
            <div className="about-host">
              <h4 className="mb-3">About host</h4>
              <div className="row">
                <div className="col-12 col-sm-2 col-md-2">
                  <img src="../assets/img/img20.png" className="img-fluid" />
                </div>
                <div className="col-12 col-sm-10 col-md-10">
                  <div className="about-host">
                    <h5>Mark harber</h5>
                    <span className="label bg-green">#1 in beach</span>
                    <span className="label bg-orange">#3 in Forest</span>
                    <p className="mt-2">
                      Holistic Life Farm offers a quaint hideaway cabin nestled
                      amongst the trees on your way to Astoria and the coast.
                      Find us about 30 minutes off I-5 across the Longview
                      bridge, 45 minutes east of Astoria and 4 miles off Highway
                      30 to the north. We are a working homestead and delight in
                      sharing our experience and offer visits with our{" "}
                      <a href="#" className="read-more">
                        READ MORE
                      </a>
                    </p>
                    <button className="btn btn-primary">
                      <svg viewBox="0 0 512 512">
                        <path
                          fill="currentColor"
                          d="M256 32C114.6 32 0 125.1 0 240c0 47.6 19.9 91.2 52.9 126.3C38 405.7 7 439.1 6.5 439.5c-6.6 7-8.4 17.2-4.6 26S14.4 480 24 480c61.5 0 110-25.7 139.1-46.3C192 442.8 223.2 448 256 448c141.4 0 256-93.1 256-208S397.4 32 256 32zm0 368c-26.7 0-53.1-4.1-78.4-12.1l-22.7-7.2-19.5 13.8c-14.3 10.1-33.9 21.4-57.5 29 7.3-12.1 14.4-25.7 19.9-40.2l10.6-28.1-20.6-21.8C69.7 314.1 48 282.2 48 240c0-88.2 93.3-160 208-160s208 71.8 208 160-93.3 160-208 160z"
                          className
                        />
                      </svg>{" "}
                      Contact
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <hr />
            <div className="reviews mb-3">
              <h4 className="fw-bold mb-3">Reviews</h4>
              {[1, 2, 3, 4, 5].map((val) => (
                <RatingView item={val} />
              ))}
            </div>
          </div>
          <div className="col-12 col-md-3 col-lg-3 col-xl-3 col-xxl-3">
            <div className="request-booking">
              <div className="bg-card  py-4 px-3">
                <h5 className="heading-font1">USD 250.00</h5>
                <p>per night (2 guests)</p>
                <hr />
                <label className="mb-0">Check-in</label>
                <select className="form-select">
                  <option>23 Dec’2021</option>
                </select>
                <hr />
                <label className="mb-0">Check-out</label>
                <select className="form-select">
                  <option>26 Dec’2021</option>
                </select>
                <hr />
                <label className="mb-0">Guests</label>
                <select className="form-select">
                  <option>2 Guests</option>
                </select>
                <hr />
                <button className="btn btn-primary w-100">
                  Request for booking
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const RatingView = (props) => {
  return (
    <div className="rev-rating-info">
      <div className="row">
        <div className="col-xl-1 col-xxl-1">
          <img src="../assets/img/img12.png" className="img-fluid" alt="..." />
        </div>
        <div className="col-xl-10 col-xxl-10">
          <div className="re-user-title">
            <h5 className>Robert mark</h5>
          </div>
          <div className="row">
            <div className="col-12 col-md-6 col-lg-6 col-xl-6 col-xxl-6">
              <div className="rating-details">
                <div className="user-rating">
                  <svg viewBox="0 0 576 512">
                    <path
                      fill="currentColor"
                      d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"
                      className
                    />
                  </svg>
                </div>
                <div className="user-rating">
                  <svg viewBox="0 0 576 512">
                    <path
                      fill="currentColor"
                      d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"
                      className
                    />
                  </svg>
                </div>
                <div className="user-rating">
                  <svg viewBox="0 0 576 512">
                    <path
                      fill="currentColor"
                      d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"
                      className
                    />
                  </svg>
                </div>
                <div className="user-rating">
                  <svg viewBox="0 0 576 512">
                    <path
                      fill="currentColor"
                      d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"
                      className
                    />
                  </svg>
                </div>
                <div className="user-rating">
                  <svg viewBox="0 0 576 512">
                    <path
                      fill="currentColor"
                      d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"
                      className
                    />
                  </svg>
                </div>{" "}
                <div className="d-inline-block">
                  <h6 className="my-auto">4.5</h6>
                </div>
              </div>
            </div>
            <div className="col-12 col-md-6 col-lg-6 col-xl-6 col-xxl-6 my-auto alignment">
              <p className="mb-0">18 Oct’ 2021 | 5:30pm</p>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-xl-10 col-xxl-10 offset-xl-1 offset-xxl-1">
          <p>
            Amazing! The tent was really cool and well ventilated. It had two
            queen size beds and great space. The views were gorgeous and not to
            mention Buddy the pig lives nearby! Mac was the friendliest and we
            felt very safe staying here. 5/5 recommend!
          </p>
          <div className="row pb-2">
            <div className="col-6 col-md-3 col-lg-3 col-xl-3 col-xxl-3">
              <button className="btn btn-secondary btn-md">
                <span className="thumb-up">
                  <svg viewBox="0 0 512 512">
                    <path
                      fill="currentColor"
                      d="M496.656 285.683C506.583 272.809 512 256 512 235.468c-.001-37.674-32.073-72.571-72.727-72.571h-70.15c8.72-17.368 20.695-38.911 20.695-69.817C389.819 34.672 366.518 0 306.91 0c-29.995 0-41.126 37.918-46.829 67.228-3.407 17.511-6.626 34.052-16.525 43.951C219.986 134.75 184 192 162.382 203.625c-2.189.922-4.986 1.648-8.032 2.223C148.577 197.484 138.931 192 128 192H32c-17.673 0-32 14.327-32 32v256c0 17.673 14.327 32 32 32h96c17.673 0 32-14.327 32-32v-8.74c32.495 0 100.687 40.747 177.455 40.726 5.505.003 37.65.03 41.013 0 59.282.014 92.255-35.887 90.335-89.793 15.127-17.727 22.539-43.337 18.225-67.105 12.456-19.526 15.126-47.07 9.628-69.405zM32 480V224h96v256H32zm424.017-203.648C472 288 472 336 450.41 347.017c13.522 22.76 1.352 53.216-15.015 61.996 8.293 52.54-18.961 70.606-57.212 70.974-3.312.03-37.247 0-40.727 0-72.929 0-134.742-40.727-177.455-40.727V235.625c37.708 0 72.305-67.939 106.183-101.818 30.545-30.545 20.363-81.454 40.727-101.817 50.909 0 50.909 35.517 50.909 61.091 0 42.189-30.545 61.09-30.545 101.817h111.999c22.73 0 40.627 20.364 40.727 40.727.099 20.363-8.001 36.375-23.984 40.727zM104 432c0 13.255-10.745 24-24 24s-24-10.745-24-24 10.745-24 24-24 24 10.745 24 24z"
                      className
                    />
                  </svg>
                </span>{" "}
                Helpful
              </button>
            </div>
            <div className="col-6 col-md-3 col-lg-3 col-xl-3 col-xxl-3 my-auto">
              <a href="#" className="rev-report">
                Report this review
              </a>
            </div>
          </div>
          <hr />
        </div>
      </div>
    </div>
  );
};
