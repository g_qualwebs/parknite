import React from "react";
import Carousel from "react-multi-carousel";
import { carousalConfig } from "../../common/Constants";

export default class SinglePropertyCarousel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      propertyData: [
        {
          src: "../assets/img/img15.png",
          title: "Shenandoah National Park",
          distance: "220 mi",
          subtitle: "Starts from 50 USD",
        },
      ],
      currentSlideIndex: 0,
    };
  }

  render() {
    const { propertyData } = this.state;
    return (
      <div className="property-banner">
        <Carousel
          ref={(el) => (this.Carousel = el)}
          responsive={carousalConfig}
          showDots={false}
          infinite={false}
          arrows={false}
          keyBoardControl={true}
          responsive={carousalConfig}
          ssr={true}
          containerClass={"owl-carousel"}
          itemClass={"owl-slider-item"}>
          {propertyData.map((item, index) => (
            <div className="row">
              <div className="col-12 col-md-3 col-lg-3 col-xl-3 col-xxl-3 my-auto">
                <div className="position-left pb-3">
                  <a href="../static/explore.html" className="back-details">
                    <svg
                      aria-hidden="true"
                      focusable="false"
                      data-prefix="fal"
                      data-icon="chevron-left"
                      role="img"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 256 512"
                      className="svg-inline--fa fa-chevron-left fa-w-8 fa-2x">
                      <path
                        fill="currentColor"
                        d="M238.475 475.535l7.071-7.07c4.686-4.686 4.686-12.284 0-16.971L50.053 256 245.546 60.506c4.686-4.686 4.686-12.284 0-16.971l-7.071-7.07c-4.686-4.686-12.284-4.686-16.97 0L10.454 247.515c-4.686 4.686-4.686 12.284 0 16.971l211.051 211.05c4.686 4.686 12.284 4.686 16.97-.001z"
                        className
                      />
                    </svg>{" "}
                    Back to listing
                  </a>
                </div>
                <div className="bg-card single-property position-left">
                  <h3>{item.title}</h3>
                  <p className="mb-1 fw-normal">
                    3.2 Mi{" "}
                    <a href="#" className="show-map">
                      Show on map
                    </a>
                  </p>
                  <h6 className="fw-normal booking-subtitle pb-3">
                    Joshua Tree National Park, Salton Sea State Recreation Area
                  </h6>
                  <div className="position-absolute content-position  bg-card text-center">
                    <h4 className="mb-0 heading-font">4.5</h4>
                    <p className="mb-0">23 Reviews</p>
                  </div>
                  <div className="position-absolute  content-position1  bg-card">
                    <svg viewBox="0 0 448 512">
                      <path
                        fill="currentColor"
                        d="M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-52.5 248.3L236.9 394.5c-7.1 7.4-18.7 7.4-25.9 0L100.5 280.3c-32.1-33.2-30.2-88.2 5.7-118.8 31.3-26.7 77.9-21.9 106.6 7.7l11.3 11.6 11.3-11.6c28.7-29.6 75.3-34.4 106.6-7.7 35.8 30.6 37.7 85.6 5.5 118.8z"
                        className
                      />
                    </svg>
                    <p className="mb-0  ms-2 d-inline-block">Save to list</p>
                  </div>
                </div>
              </div>
              <div className="col-12 col-md-9 col-lg-9 col-xl-9 col-xxl-9">
                <div className="pro-banner-img">
                  <img
                    src="../assets/img/img15.png"
                    className="img-fluid"
                    alt="..."
                  />
                </div>
                <div className="share-icon">
                  <a href="#">
                    <svg viewBox="0 0 448 512">
                      <path
                        fill="currentColor"
                        d="M352 320c-25.6 0-48.9 10-66.1 26.4l-98.3-61.5c5.9-18.8 5.9-39.1 0-57.8l98.3-61.5C303.1 182 326.4 192 352 192c53 0 96-43 96-96S405 0 352 0s-96 43-96 96c0 9.8 1.5 19.6 4.4 28.9l-98.3 61.5C144.9 170 121.6 160 96 160c-53 0-96 43-96 96s43 96 96 96c25.6 0 48.9-10 66.1-26.4l98.3 61.5c-2.9 9.4-4.4 19.1-4.4 28.9 0 53 43 96 96 96s96-43 96-96-43-96-96-96zm0-272c26.5 0 48 21.5 48 48s-21.5 48-48 48-48-21.5-48-48 21.5-48 48-48zM96 304c-26.5 0-48-21.5-48-48s21.5-48 48-48 48 21.5 48 48-21.5 48-48 48zm256 160c-26.5 0-48-21.5-48-48s21.5-48 48-48 48 21.5 48 48-21.5 48-48 48z"
                        className
                      />
                    </svg>
                  </a>
                </div>
              </div>
            </div>
          ))}
        </Carousel>
        <div className="slider_nav navigate">
          <button
            className="button-border-clear"
            onClick={() => {
              this.setState(
                {
                  currentSlideIndex:
                    this.state.currentSlideIndex != 0
                      ? this.state.currentSlideIndex - 1
                      : 0,
                },
                () => {
                  this.Carousel.goToSlide(this.state.currentSlideIndex);
                }
              );
            }}>
            <div className="am-prev">
              <img
                src="../assets/img/left1.png"
                className="img-fluid"
                alt="..."
              />
            </div>
          </button>
          <button
            className="button-border-clear"
            onClick={() => {
              this.setState(
                {
                  currentSlideIndex:
                    this.state.currentSlideIndex <
                    this.state.propertyData.length - 1
                      ? this.state.currentSlideIndex + 1
                      : this.state.currentSlideIndex,
                },
                () => {
                  this.Carousel.goToSlide(this.state.currentSlideIndex);
                }
              );
            }}>
            <div className="am-next">
              <img
                src="../assets/img/right1.png"
                className="img-fluid"
                alt="..."
              />
            </div>
          </button>
        </div>
      </div>
    );
  }
}
