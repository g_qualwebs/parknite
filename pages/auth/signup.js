import React from "react";
import Head from "next/head";
import Header from "../../components/common/Header";
import Footer from "../../components/common/Footer";

class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
    };
  }

  render() {
    return (
      <>
        <Head>
          <title>Parknite | Login</title>
        </Head>
        <Header isOverlapping={true} showBanner={true} />
        <section>
          <div className="container">
            <div className="row">
              <div className="col-12 col-md-5 col-lg-5 col-xl-7 col-xxl-7 mx-auto">
                <form>
                  <div className="form py-ssm">
                    <div className="row">
                      <div className="col-12 col-sm-11 mx-auto">
                        <h3 className="text-center fw-bold mb-3">
                          Create account
                        </h3>
                        <div className="row justify-content-center">
                          <div className="col-3 col-sm-2 pe-0">
                            <div className="circle">
                              <div className="fb">
                                <svg viewBox="0 0 320 512">
                                  <path
                                    fill="currentColor"
                                    d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"
                                    className
                                  />
                                </svg>
                              </div>
                            </div>
                          </div>
                          <div className="col-3 col-sm-2 ps-0">
                            <div className="circle">
                              <div className="google">
                                <svg viewBox="0 0 488 512">
                                  <path
                                    fill="currentColor"
                                    d="M488 261.8C488 403.3 391.1 504 248 504 110.8 504 0 393.2 0 256S110.8 8 248 8c66.8 0 123 24.5 166.3 64.9l-67.5 64.9C258.5 52.6 94.3 116.6 94.3 256c0 86.5 69.1 156.6 153.7 156.6 98.2 0 135-70.4 140.8-106.9H248v-85.3h236.1c2.3 12.7 3.9 24.9 3.9 41.4z"
                                    className
                                  />
                                </svg>
                              </div>
                            </div>
                          </div>
                        </div>
                        <hr />
                        <p className="text-center sub-title">
                          or create account with your email id
                        </p>
                      </div>
                    </div>
                    <div className="row input-forms">
                      <div className="col-12 col-sm-6 mb-3">
                        <input
                          type="text"
                          placeholder="First name"
                          className="form-control"
                          id="fname"
                        />
                      </div>
                      <div className="col-12 col-sm-6 mb-3">
                        <input
                          type="text"
                          placeholder="Last name"
                          className="form-control"
                          id="lname"
                        />
                      </div>
                      <div className="col-12 col-sm-6 mb-3">
                        <input
                          type="email"
                          placeholder="Email address"
                          className="form-control"
                          id="email"
                        />
                      </div>
                      <div className="col-12 col-sm-6 mb-3">
                        <input
                          type="text"
                          placeholder="Company name"
                          className="form-control"
                          id="cname"
                        />
                      </div>
                      <div className="col-12 col-sm-6 mb-3">
                        <select
                          className="form-select"
                          aria-label="Default select example">
                          <option selected>Country</option>
                        </select>
                      </div>
                      <div className="col-12 col-sm-6 mb-3">
                        <input
                          type="number"
                          placeholder="Contact number"
                          className="form-control"
                          id="contact"
                        />
                      </div>
                      <div className="col-12 col-sm-6 mb-3">
                        <input
                          type="password"
                          placeholder="Password"
                          className="form-control"
                          id="password"
                        />
                      </div>
                      <div className="col-12 col-sm-6 mb-3">
                        <input
                          type="password"
                          placeholder="Confirm password"
                          className="form-control"
                          id="cpassword"
                        />
                      </div>
                    </div>
                    <div className="content">
                      <div className="row">
                        <div className="col-sm-12">
                          <div className="custom-checkbox mb-2">
                            <input type="checkbox" id="rating1" />
                            <label htmlFor="rating1">
                              Get latest news and updates from Parknite and
                              improve your travelling experience.
                            </label>
                          </div>
                        </div>
                        <div className="col-sm-12">
                          <div className="clicking-ac">
                            <p className="mb-1">
                              By clicking on “Create Account” I agree to
                              Parknite’s <a href="#">terms and conditions</a>,{" "}
                              <a href="#">privacy policy</a>.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="py-3 text-center">
                      <a className="btn btn-secondary btn-lg w-50" href="#">
                        Create account
                      </a>
                    </div>
                    <div className="text-center join-content">
                      <h6 className="d-inline-block">
                        Already have an acocunt?
                      </h6>
                      <a href="#" className="d-inline-block">
                        Login
                      </a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>

        <Footer />
      </>
    );
  }
}

export default Signup;
