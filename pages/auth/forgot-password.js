import React from "react";
import Head from "next/head";
import Header from "../../components/common/Header";
import Footer from "../../components/common/Footer";

class ForgotPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
    };
  }

  render() {
    return (
      <>
        <Head>
          <title>Parknite | Login</title>
        </Head>
        <Header isOverlapping={true} showBanner={true} />
        <section>
          <div className="container">
            <div className="row">
              <div className="col-12 col-md-5 col-lg-5 col-xl-4 col-xxl-4 mx-auto">
                <form>
                  <div className="form py-ssm">
                    <div className="row">
                      <div className="col-12 col-sm-12 mx-auto">
                        <h3 className="text-center fw-bold mb-3">
                          Forgot Password
                        </h3>
                        <p className="text-center">
                          Enter your registered email address to reset your
                          account password.
                        </p>
                      </div>
                    </div>
                    <div className="input-forms">
                      <div className="mb-2">
                        <input
                          type="email"
                          placeholder="Your registered email Id"
                          className="form-control"
                          id="email"
                        />
                      </div>
                    </div>
                    <div className="py-3">
                      <a className="btn btn-secondary btn-lg w-100" href="#">
                        Confirm email
                      </a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>
        <Footer />
      </>
    );
  }
}

export default ForgotPassword;
