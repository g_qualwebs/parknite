import React from "react";
import Head from "next/head";
import Header from "../../components/common/Header";
import Footer from "../../components/common/Footer";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
    };
  }

  render() {
    return (
      <>
        <Head>
          <title>Parknite | Login</title>
        </Head>
        <Header isOverlapping={true} showBanner={true} />
        <section>
          <div className="container">
            <div className="row">
              <div className="col-12 col-md-5 col-lg-5 col-xl-4 col-xxl-4 mx-auto">
                <form>
                  <div className="form py-ssm">
                    <div className="row">
                      <div className="col-12 col-sm-11 mx-auto">
                        <h3 className="text-center fw-bold mb-3">Login</h3>
                        <div className="row justify-content-center">
                          <div className="col-3 col-sm-2">
                            <div className="circle">
                              <div className="fb">
                                <svg viewBox="0 0 320 512">
                                  <path
                                    fill="currentColor"
                                    d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"
                                    className
                                  />
                                </svg>
                              </div>
                            </div>
                          </div>
                          <div className="col-3 col-sm-2">
                            <div className="circle">
                              <div className="google">
                                <svg viewBox="0 0 488 512">
                                  <path
                                    fill="currentColor"
                                    d="M488 261.8C488 403.3 391.1 504 248 504 110.8 504 0 393.2 0 256S110.8 8 248 8c66.8 0 123 24.5 166.3 64.9l-67.5 64.9C258.5 52.6 94.3 116.6 94.3 256c0 86.5 69.1 156.6 153.7 156.6 98.2 0 135-70.4 140.8-106.9H248v-85.3h236.1c2.3 12.7 3.9 24.9 3.9 41.4z"
                                    className
                                  />
                                </svg>
                              </div>
                            </div>
                          </div>
                        </div>
                        <hr />
                        <p className="text-center  sub-title">
                          or use your registered email address
                        </p>
                      </div>
                    </div>
                    <div className="input-forms">
                      <div className="mb-3">
                        <input
                          type="email"
                          placeholder="Your registered email"
                          className="form-control"
                          id="email"
                        />
                      </div>
                      <div className="mb-3 password">
                        <input
                          type="password"
                          placeholder="Enter your password"
                          className="form-control"
                          id="password"
                        />
                        <svg viewBox="0 0 576 512">
                          <path
                            fill="currentColor"
                            d="M572.52 241.4C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288 448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 400a144 144 0 1 1 144-144 143.93 143.93 0 0 1-144 144zm0-240a95.31 95.31 0 0 0-25.31 3.79 47.85 47.85 0 0 1-66.9 66.9A95.78 95.78 0 1 0 288 160z"
                            className
                          />
                        </svg>
                      </div>
                    </div>
                    <div className="content">
                      <div className="row">
                        <div className="col-sm-6">
                          <div className="custom-checkbox">
                            <input type="checkbox" id="rating1" />
                            <label htmlFor="rating1">Remember me</label>
                          </div>
                        </div>
                        <div className="col-sm-6 text-end">
                          <div className="forget-psd">
                            <a href="#">Forget password?</a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="py-3">
                      <a className="btn btn-secondary btn-lg w-100" href="#">
                        Login
                      </a>
                    </div>
                    <div className="text-center join-content">
                      <h6 className="d-inline-block">New on parknite?</h6>
                      <a href="#" className="d-inline-block">
                        Join now
                      </a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>
        <Footer />
      </>
    );
  }
}

export default Login;
