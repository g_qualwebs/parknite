import React from "react";
import Layout from "../components/common/Layout";
import ExploreNearby from "../components/home/ExploreNearby";
import FindUs from "../components/home/FindUs";
import HomeBanner from "../components/home/HomeBanner";
import Head from "next/head";

import { withSnackbar } from "notistack";

class Home extends React.Component {
  constructor(props) {
    super();
  }

  render() {
    return (
      <>
        <Layout>
          <Head>Parknite | Home</Head>
          <div>
            <HomeBanner />
            <ExploreNearby />
            <FindUs />
          </div>
        </Layout>
      </>
    );
  }
}

export default withSnackbar(Home);
