import React from "react";
import Header from "../../components/common/Header";
import Select from "react-select";

import DayPickerInput from "react-day-picker/DayPickerInput";
import "react-day-picker/lib/style.css";

import { DestinationCardView } from "../../components/home/ExploreNearby";
import { customStylesDropdown } from "../../components/common/Constants";
import { Accordion } from "react-bootstrap";
import Form from "react-bootstrap/Form";

export default class Explore extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentSlideIndex: 0,
      searchData: {
        location: "",
        nearBy: "",
        checkIn: "",
        checkOut: "",
        guests: "",
      },
      exploreData: [
        {
          src: "../assets/img/img7.jpg",
          title: "Beach",
          subtitle: "15 properties",
        },
        {
          src: "../assets/img/img7.jpg",
          title: "Mountains",
          subtitle: "15 properties",
        },
        {
          src: "../assets/img/img7.jpg",
          title: "Dessert",
          subtitle: "15 properties",
        },
        {
          src: "../assets/img/img7.jpg",
          title: "Forrest",
          subtitle: "15 properties",
        },
      ],

      nearbyData: [
        { value: "location 1", label: "location 1" },
        { value: "location 2", label: "location 2" },
        { value: "location 3", label: "location 3" },
      ],
      guestData: [
        { value: "1 Guest", label: "1 Guest" },
        { value: "2 Guest", label: "2 Guest" },
        { value: "3 Guest", label: "3 Guest" },
      ],
      filterData: [
        {
          id: 1,
          title: "Pricing",
          data: [
            { id: 1, value: "$100", isEnabled: false },
            { id: 2, value: "$200", isEnabled: false },
            { id: 3, value: "$300", isEnabled: false },
          ],
        },
        {
          id: 2,
          title: "Rating",
          data: [
            { id: 1, value: "4.5 & above", isEnabled: false },
            { id: 2, value: "4 & above", isEnabled: false },
            { id: 3, value: "3 & above", isEnabled: false },
          ],
        },
        {
          id: 3,
          title: "Vehicle Type",
          data: [
            { id: 1, value: "Mini SUV", isEnabled: false },
            { id: 2, value: "Minibus", isEnabled: false },
            { id: 3, value: "Truck", isEnabled: false },
          ],
        },
        {
          id: 4,
          title: "Site Type",
          data: [
            { id: 1, value: "Beach", isEnabled: false },
            { id: 2, value: "Island", isEnabled: false },
            { id: 3, value: "Mountains", isEnabled: false },
          ],
        },
        {
          id: 5,
          title: "No. of groups",
          data: [
            { id: 1, value: "1", isEnabled: false },
            { id: 2, value: "2", isEnabled: false },
            { id: 3, value: "3", isEnabled: false },
          ],
        },
        {
          id: 6,
          title: "Amenities",
          data: [
            { id: 1, value: "Amenity 1", isEnabled: false },
            { id: 2, value: "Amenity 2", isEnabled: false },
            { id: 3, value: "Amenity 3", isEnabled: false },
          ],
        },
        {
          id: 7,
          title: "Activities",
          data: [
            { id: 1, value: "Activity 1", isEnabled: false },
            { id: 2, value: "Activity 2", isEnabled: false },
            { id: 3, value: "Activity 3", isEnabled: false },
          ],
        },
        {
          id: 8,
          title: "Terrain",
          data: [
            { id: 1, value: "Terrain 1", isEnabled: false },
            { id: 2, value: "Terrain 2", isEnabled: false },
            { id: 3, value: "Terrain 3", isEnabled: false },
          ],
        },
        {
          id: 9,
          title: "Cancellation flexibility",
          data: [
            { id: 1, value: "Within 24 hours", isEnabled: false },
            { id: 2, value: "Within 1 day", isEnabled: false },
            { id: 3, value: "Within 2 day", isEnabled: false },
          ],
        },
        {
          id: 10,
          title: "Booking type",
          data: [
            { id: 1, value: "Type 1", isEnabled: false },
            { id: 2, value: "Type 2", isEnabled: false },
            { id: 3, value: "Type 3", isEnabled: false },
          ],
        },
      ],
      isEmpty: true,
      isDisabled: false,
    };

    this.updateMasterState = this.updateMasterState.bind(this);
    this.handleDayChange = this.handleDayChange.bind(this);
    this.handleDropdownChange = this.handleDropdownChange.bind(this);
    this.toggleRadioEnabled = this.toggleRadioEnabled.bind(this);
  }

  updateMasterState(e) {
    const searchData = this.state.searchData;
    searchData[e.target.name] = e.target.value;
    this.setState({ searchData });
  }

  handleDayChange(selectedDay, modifiers, dayPickerInput) {
    const input = dayPickerInput.getInput();
    const searchData = this.state.searchData;

    this.setState({
      selectedDay,
    });
  }

  handleDropdownChange(e, name) {
    let searchData = this.state.searchData;
    searchData[name] = e;
    this.setState({ searchData });
  }

  SearchViewSection(props) {
    let that = this;
    return (
      <section className="bg-blue py-1">
        <div className="container">
          <div className="row">
            <div className="col-12 col-md-11">
              <form className="search-form">
                <div className="row grid-divider">
                  <div className="col-sm-2">
                    <label>Location</label>
                    <input
                      className="form-control"
                      placeholder="Search location"
                      value={props.searchData.location}
                      name={"location"}
                      onChange={props.that.updateMasterState}
                    />
                  </div>
                  <div className="col-sm-2">
                    <label>Find Near</label>
                    <div>
                      <Select
                        options={props.nearbyData}
                        styles={customStylesDropdown}
                        menuColor={"#ffffff"}
                        placeholder={"Select.."}
                        value={props.searchData.nearBy}
                        instanceId={"test"}
                        onChange={(e) => {
                          props.that.handleDropdownChange(e, "nearBy");
                        }}
                      />
                    </div>
                  </div>
                  <div className="col-sm-2">
                    <label>Check-in</label>
                    <div>
                      <DayPickerInput
                        inputProps={{
                          style: {
                            width: 150,
                            backgroundColor: "transparent",
                            border: "none",
                            borderColor: "transparent",
                            fontSize: 14,
                            color: "#ffffff",
                          },
                        }}
                        name={"checkIn"}
                        placeholder={`23 Dec'2021`}
                        //  value={props.searchData.checkIn}
                        onDayChange={props.that.handleDayChange}
                      />
                    </div>
                  </div>
                  <div className="col-sm-2">
                    <label>Check-out</label>
                    <div>
                      <DayPickerInput
                        inputProps={{
                          style: {
                            width: 150,
                            backgroundColor: "transparent",
                            border: "none",
                            borderColor: "transparent",
                            fontSize: 14,
                            color: "#ffffff",
                          },
                        }}
                        placeholder={`23 Dec'2021`}
                        name={"checkOut"}
                        // value={props.searchData.checkOut}
                        onDayChange={props.that.handleDayChange}
                      />
                    </div>
                  </div>
                  <div className="col-sm-2">
                    <label>Guests</label>
                    <div>
                      <Select
                        options={props.guestData}
                        styles={customStylesDropdown}
                        menuColor={"#ffffff"}
                        placeholder={"Select.."}
                        value={props.searchData.guests}
                        onChange={(e) => {
                          props.that.handleDropdownChange(e, "guests");
                        }}
                      />
                    </div>
                  </div>
                  <div className="col-sm-2 my-auto">
                    <button className="btn btn-secondary">Search</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    );
  }

  toggleRadioEnabled(e, index, id) {
    let filterData = this.state.filterData;
    filterData[index].data[id].isEnabled =
      !filterData[index].data[id].isEnabled;
    this.setState({ filterData });
  }

  render() {
    let { searchData, nearbyData, guestData } = this.state;
    return (
      <>
        <Header />
        <this.SearchViewSection
          searchData={searchData}
          nearbyData={nearbyData}
          guestData={guestData}
          that={this}
        />
        <section>
          <div className="container">
            <div className="row">
              <div className="col-12 col-md-3 col-lg-3 col-xl-3">
                <div className="bg-card filter my-3 p-3">
                  <div className="row">
                    <div className="col-sm-10 text-right filter-title mb-0 my-auto">
                      <h5 className="mb-0">50 properties</h5>
                    </div>
                    <div className="col-sm-2 filter-icon">
                      <svg viewBox="0 0 512 512">
                        <path
                          fill="currentColor"
                          d="M0 80v352c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V80c0-26.51-21.49-48-48-48H48C21.49 32 0 53.49 0 80zm240-16v176H32V80c0-8.837 7.163-16 16-16h192zM32 432V272h208v176H48c-8.837 0-16-7.163-16-16zm240 16V272h208v160c0 8.837-7.163 16-16 16H272zm208-208H272V64h192c8.837 0 16 7.163 16 16v160z"
                        />
                      </svg>
                    </div>
                  </div>
                  <hr />
                  <Accordion className={"accordion"} flush>
                    {this.state.filterData.map((item, index1) => (
                      <Accordion.Item
                        key={item.title}
                        eventKey={item.id}
                        className="accordion-item">
                        <Accordion.Header
                          id={item.id}
                          className="accordion-header">
                          {item.title}
                        </Accordion.Header>
                        <Accordion.Body>
                          <div class="accordion-body rating-details">
                            {item.data.map((val, index2) => (
                              <Form key={val.value} id={index1}>
                                <div key={val.id} className="custom-radio">
                                  <Form.Check
                                    inline
                                    id={`inline-${index2}`}
                                    type="radio"
                                    checked={val.isEnabled}
                                    onClick={(val) => {
                                      this.toggleRadioEnabled(
                                        val,
                                        index1,
                                        index2
                                      );
                                    }}
                                    label={val.value}
                                  />
                                </div>
                              </Form>
                            ))}
                          </div>
                        </Accordion.Body>
                      </Accordion.Item>
                    ))}
                  </Accordion>
                </div>
              </div>
              <div className="col-12 col-md-9 col-lg-9 col-xl-9">
                <div className="explore my-3">
                  <div className="row">
                    {this.state.exploreData.map((item, index) => (
                      <ExploreCardView key={item.title} item={item} />
                    ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        
      </>
    );
  }
}

const ExploreCardView = (props) => {
  return (
    <div className="col-12 col-md-4 col-lg-4 col-xl-4 col-xxl-4">
      <div className="card-details p-ssm">
        <div className="card-img">
          <img src="../assets/img/img8.png" />
        </div>
        <div className="card-body">
          <div className="bg-card">
            <div className="card-padding">
              <h5 className="mb-1">Shenandoah National Park</h5>
              <div className="d-flex justify-content-between">
                <div>
                  <p className="fw-normal mb-2">220 mi</p>
                </div>
                <div className="rating">
                  <svg viewBox="0 0 576 512">
                    <path
                      fill="currentColor"
                      d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"
                      className
                    />
                  </svg>{" "}
                  <p className="d-inline-block mb-0">4.5</p>
                </div>
              </div>
              <h6>Starts from 50 USD</h6>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
