import React from "react";
import Carousel from "react-multi-carousel";
import { carousalConfig } from "../../components/common/Constants";
import Header from "../../components/common/Header";
import { DestinationCardView } from "../../components/home/ExploreNearby";
import PropertyDetails from "../../components/host/singleProperty/PropertyDetails";
import SinglePropertyCarousel from "../../components/host/singleProperty/SinglePropertyCarousel";

export default class SingleProperty extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { propertyData } = this.state;
    return (
      <>
        <Header isOverlapping={false} />
        <section>
          <SinglePropertyCarousel />
          <PropertyDetails />
          <div className="container">
            <div className="destination mb-ssm">
              <div className="container">
                <div className="row">
                  <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                    <div className="main-heading mb-ssm">
                      <h2 className="fw-bold mb-1">Related Properties</h2>
                      <h5 className="fw-normal mb-0">
                        Discover unique spots near you to enjoy your day.
                      </h5>
                    </div>
                  </div>
                </div>
                <div className="row">
                  {[1, 2, 3, 4, 5, 6, 7, 8].map((item, index) => (
                    <DestinationCardView />
                  ))}
                </div>
              </div>
            </div>
          </div>
        </section>
      </>
    );
  }
}
